import React, { useContext } from 'react'
import { WContext } from '../Context/Mycontext';

function SearchRes() {
  const { data } = useContext(WContext);

  console.log(data);
  return (
    <>
      <main className='mt-5'>
        <h2 className='text-white text-center'>
          City Info
        </h2>
        {data ? <table className="table text-center text-white">
          <thead>
            <tr >
              <th scope="col">City name</th>
              <th scope="col">humidity</th>
              <th scope="col">Rainy</th>
              <th scope="col">Snowy</th>
              <th scope="col">Weather</th>
              <th scope="col">Weather description</th>
              <th scope="col">Wind Deg</th>
              <th scope="col">Wind Speed <i className='fas fa-wind'></i></th>


            </tr>
          </thead>
          <tbody className='text-center'>

            <tr>
              <th >{data.name}</th>
              <td>{data.main.humidity} *</td>
              <td >{data.rain ? <i className='fa fa-check'></i> : <i className='fa fa-times'></i>}</td>
              <td>{data.snow ? <i className='fa fa-check'></i> : <i className='fa fa-times'></i>}</td>
              <td>{data.weather[0].main}</td>
              <td>{data.weather[0].description}</td>
              <td>{data.wind.deg}</td>
              <td>{data.wind.speed}</td>
            </tr>
          </tbody>
        </table> : <h3 className='text-info text-center mt-5'>No Data</h3>}
        
      </main>

    </>
  )
}

export default SearchRes