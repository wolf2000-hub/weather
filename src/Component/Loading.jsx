import React from 'react'
import SpinnerGIF from "../assets/Spinner.gif";

function Loading() {
    return (
        <>
            <img
                src={SpinnerGIF}
                className="d-block m-auto py-5"
                style={{ width: "100px" }}
                alt="Spiner"
            />
        </>
    );
}

export default Loading