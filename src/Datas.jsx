import React, { useState } from 'react'

import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Loading from './Component/Loading';
import './assets/Style.css';
import { WContext } from './Context/Mycontext';
import SearchRes from './Component/SearchRes';
import { axios_city_w } from './Component/axios/Axios';
function Datas() {
  const [data, setDate] = useState();
  const [loading, setLoading] = useState(false);
  const [active, setActive] = useState(false);
  return (
    <>
      {loading ? <Loading />
        :
        <div>
          <section className="image__background ">
            <div className="mask d-flex align-items-center gradient-custom-3 h-100">
              <div className="container h-100">
                <Formik
                  initialValues={{
                    search: '',
                  }}
                  validationSchema={Yup.object().shape({
                    search: Yup.string()
                      .required('search is required'),
                  })}
                  onSubmit={async (values) => {
                    const search = values.search;
                    try {
                      axios_city_w(search)
                        .then(res => {
                          setLoading(true);
                          setDate(res.data.list[0]);
                          setLoading(false);
                          setActive(true);
                        }).catch(err => {
                          alert(err.response.data.errors);
                          setLoading(false);
                        })
                    } catch (err) {
                      console.log(err);
                    }
                  }}
                  render={({ errors, status, touched }) => (
                    <Form>
                      <div className="mt-5">

                        <div className="row d-flex justify-content-center align-items-center h-100">
                          <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                            <div className="card shadow-lg login_Card">
                              <div className="card-body p-5">
                                <h2 className="text-uppercase text-center mb-5">Search Your Location</h2>
                                <div className="form-outline mb-4">
                                  <Field name="search" type="text" placeholder="City Name" id="form3Example3cg" className={'form-control form-control-lg' + (errors.search && touched.search ? ' is-invalid' : '')} />
                                  <ErrorMessage name="search" component="div" className="invalid-feedback" />
                                </div>
                                <div className="d-flex justify-content-center">
                                  <button type="submit" className="btn btn-outline-info mx-2">Search</button>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


                    </Form>
                  )}
                />
                {active ?
                  <WContext.Provider
                    value={{
                      data,
                    }}
                  >
                    <SearchRes />
                  </WContext.Provider> : null}

              </div>
            </div>
          </section>
        </div>

      }


    </>
  )
}

export default Datas